const url = require('url');

//Get
exports.sampleRequest = function (req, res) {
//Get request will always return world
//    const reqUrl = url.parse(req.url, true);
//    var name = 'World';
//    if (reqUrl.query.name) {
//        name = reqUrl.query.name
//    }

    var response = {
        "text": "world"
    };

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(response));
};

//Post
exports.testRequest = function (req, res) {
    body = '';

    req.on('data', function (chunk) {
        body += chunk;
    });

    req.on('end', function () {

        postBody = JSON.parse(body);
//Responce for POST is always world
        var response = {
            "data": "world"
        };

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
    });
};

exports.invalidRequest = function (req, res) {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain');
    res.end('error:invalid OU value');
};
